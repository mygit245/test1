#!/usr/bin/python2.2
#
# Copyright 2004 Google Inc.
# All Rights Reserved.

"""
Python helper script for gtags.vim.

Note that this cannot be executed by a normal Python interpreter. It
imports this vim module, and hence needs to be executed by a Python
interpreter embedded within vim (or gvim).
"""

__author__ = "Laurence Gonsalves"
__version__ = """$Id: //depot/opensource/gtags/gtags_vim.py#2 $"""

import vim
import os
import sys
import re
import socket

sys.path.append(vim.eval('g:google_tags_pydir'))

import gtags

VIM_TO_GTAGS_TYPES = {
  'c' : 'c++',
  'cpp' : 'c++',
  'java' : 'java',
  'python' : 'python',
}

def GetFiletype():
  # determine filetype based on vim's current filetype, then
  # g:google_tags_default_filetype, and then finally assume C++ if we
  # still can't find anything reasonable.
  filetype = vim.eval('&ft')
  filetype = VIM_TO_GTAGS_TYPES.get(filetype)
  if not filetype:
    filetype = vim.eval('g:google_tags_default_filetype')
  if not filetype:
    filetype = 'c++'
  return filetype


def SoftenServerErrors(f, default):
  """
  Calls f (with no arguments) and returns its result. If f raises a
  socket error, generates a VIM warning messages and returns the
  specified default value instead.
  """
  try:
    return f()
  except socket.error:
    vim.command('echohl WarningMsg')
    vim.command('echomsg "GTAGS server down"')
    vim.command('echohl None')
    return default


def FindExactTag(name):
  filetype = GetFiletype()
  return SoftenServerErrors(lambda: gtags.find_tag(filetype, name), [])


def FindTagByPattern(pattern):
  filetype = GetFiletype()
  return SoftenServerErrors(lambda: gtags.find_matching_tags(filetype,
                                                             pattern), [])

def ResolveFilename(fnam):
  # The client's google3 directory is assumed to be the top-most
  # ancestor of the current directory named google3. If there is no
  # ancestor of the current directory named google3, then we just use
  # the current directory.
  google3 = re.sub('/google3/.*', '/google3/', os.getcwd() + '/')

  # Filenames supplied by the tags server are relative to google3. For
  # each file, we first look for it under the client's google3
  # directory, and then the various /home/build google3 directorys (in
  # order of decreasing permission requirements).  The first one we can
  # find is the file name that gets written to the output file.  If
  # neither can be found, then the original relative file name is
  # written.
  path = [google3,
          '/home/build/public/google3/',
          '/home/build/nonconf/google3/',
          '/home/build/google3/']

  for dir in path:
    full_fnam = os.path.join(dir, fnam)
    if os.path.exists(full_fnam):
      return full_fnam
  return fnam

def WriteTagFile(name):
  """
  Writes a vi-format tags file based on the supplied symbol name.
  """
  # The file name for output is read from the vim variable g:google_tags.
  tagfile = vim.eval('g:google_tags')

  tags = FindExactTag(name)

  # write out the tags file
  f = open(tagfile, 'w')
  for tag in tags:
    fnam = ResolveFilename(tag.filename_)
    print >> f, '%s\t%s\t%s' % (tag.tag_, fnam, tag.lineno_)


def SortedAndUniquified(seq):
  m = {}
  for v in seq:
    m[v] = 1
  seq = m.keys()
  seq.sort()
  return seq

def GtagComplete():
  arglead = vim.eval('a:ArgLead')

  tags = FindTagByPattern(arglead + '.*')
  result = [tag.tag_ for tag in tags]
  # TODO: make sure result has no magic chars?

  result = SortedAndUniquified(result)

  vim.command('let r="%s"' % '\\n'.join(result))


def Gtgrep():
  pattern = vim.eval('a:pattern')
  outfnam = vim.eval('g:google_grep')
  tags = FindTagByPattern(pattern)
  f = open(outfnam, 'w')
  for tag in tags:
    fnam = ResolveFilename(tag.filename_)
    print >>f, '%s:%s:%s' % (fnam, tag.lineno_, tag.snippet_)
