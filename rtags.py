#!/usr/bin/python2.3
#
# Copyright 2004 Google Inc.
# All Rights Reserved.

"""
Produce index files for reverse call graph data (i.e., function calls)

Generates data in etags format:
etags file format is as follows:
^L
filename,no of chars in file
line snippet 0x7f identifier 0x01 lineno, char offset
"""

__author__ = "Piaw Na"

import sys
import re
import os

from optparse import OptionParser

funcall = re.compile('([a-zA-Z_][a-zA-Z0-9_]*)\(')

# print all matches on a line
# this is required because function call arguments can themselves
# be function calls, ad-infinitem, and there can be multiple
# function calls on a line for instance f(g) + h(g)
def print_matches(line, lineno, offset):
  dict = {}          # use a dictionary to suppress duplicates
  matches = re.findall(funcall,line)
  for m in matches:
    if not dict.has_key(m):
      print line + chr(0x7f) + m + chr(0x01) + str(lineno) + \
            ',' + str(offset)
      dict[m] = 1


parser = OptionParser('usage: rtags.py <indexfile> <files to index>...')
parser.add_option('-a', action='store_true', dest='append', default=True,
                  help='Append to index file')
parser.add_option('-w', action='store_false', dest='append',
                  help='Overwrite index file')
(options, args) = parser.parse_args()
if len(args) < 1:
  parser.error('insufficient arguments')
  sys.exit(0)

if options.append:
  writemode = 'a'
else:
  writemode = 'w'

ofile = open(args[0], writemode)
sys.stdout = ofile

for filename in args[1:]:
  lineno = 1
  offset = 0
  stat = os.open(filename, os.O_RDONLY)
  filesize = os.fstat(stat).st_size
  os.close(stat)
  print '\f'
  print filename + ',' + str(filesize)
  for line in file(filename):
    line = line[0:len(line)-1]
    print_matches(line, lineno, offset)
    lineno += 1
    offset += len(line)
sys.exit(0)
