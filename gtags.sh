#!/bin/sh
# Copyright 2004 Google Inc.
# All Rights Reserved.
#
# Author: Sitaram Iyer

# Command-line interface to gtags.
# (tip: may be convenient to alias 't' to gtags.sh)
#
# Example usage:                   gtags.sh -w -r mapreduce Output
# which is equivalent to:          gtags.sh --color ^Output$ | grep \ mapreduce/
#
# For invocations and definitions: gtags.sh -i -w -r mapreduce Output
# Emit vi cmds for cut-and-paste:  gtags.sh -v -w -r mapreduce Output

# TODO: decipher .proto files just as gtags.el does


function usage() {
cat <<EOF
Command-line interface to gtags.

Usage: gtags.sh [flags] tag ...
flags:
  -L|--lang c++|java|python|szl	# language of source files to look in
  -w|--word			# match whole word only (same as ^tag$)
  -i|--invocations		# match invocations *and* definitions of tag
  -v|--vi  |  -l|--less		# emit vi or less commands instead of file:line
  -r|--restrict <google3_dir>	# restrict matches to google3/<dir>/
  --color | --nocolor		# override "use ansi colors if isatty(stdout)"

(tip: may be convenient to alias 't' to gtags.sh)

Example usage:                   gtags.sh -w -r mapreduce Output
which is equivalent to:          gtags.sh --color ^Output$ | grep \ mapreduce/

For invocations and definitions: gtags.sh -i -w -r mapreduce Output
Emit vi cmds for cut-and-paste:  gtags.sh -v -w -r mapreduce Output
EOF
}

lang=c++		# c++ java python szl
word=0			# match full word
invocations=0		# whether to search for definitions or invocations too
vi=0			# output vi command instead of "* file:offset"
less=0			# output less command instead of "* file:offset"
restrictcmd="cat"	# cmd that restricts to a directory in google3

color=1
if perl -e 'exit -t STDOUT'; then color=0; fi  # stdout is not a terminal


opts=`getopt -o L:wivlr: \
      --long word,invocations,vi,less,lang:,restrict:,color,nocolor \
      -n 'gtags.sh' -- "$@"`

eval set -- "$opts"
while true ; do
  case "$1" in
    -L|--lang)		lang=$2; shift 2 ;;
    -w|--word)		word=1; shift ;;
    -i|--invocations)	invocations=1; shift ;;
    -v|--vi)		vi=1; shift;;
    -l|--less)		less=1; shift ;;
    -r|--restrict)	restrictcmd="grep '\\ $2/'"; shift 2 ;;
    --color)		color=1; shift ;; # color regardless of isatty(stdout)
    --nocolor)		color=0; shift ;; # nocolor regardless of isatty(stdout)
    --)			shift ; break ;;
    *)			echo "Internal error!" ; exit 1 ;;
  esac
done

if [ "$*" = "" ]; then
  usage
  exit 1
fi

# "* file:line" instead of "file:line", so that grep \ dir/ works uniformly
outfmt="'* ' + result.filename_ + ':' + result.lineno_"
if [ "$less" = "1" ]; then
  outfmt="'less +' + result.lineno_ + ' ' + result.filename_"
elif [ "$vi" = "1" ]; then
  outfmt="'vi +' + result.lineno_ + ' ' + result.filename_"
fi

if [ "$color" = "1" ]; then   # ansi escape sequences for bright colors
  red="'\033[1;31m'"
  green="'\033[1;32m'"
  yellow="'\033[1;33m'"
  blue="'\033[1;34m'"
  magenta="'\033[1;35m'"
  cyan="'\033[1;36m'"
  white="'\033[1;37m'"
  black="'\033[0m'"
else
  red="''"
  green="''"
  yellow="''"
  blue="''"
  magenta="''"
  cyan="''"
  white="''"
  black="''"
fi

columns=${COLUMNS:-80}  # columns=$COLUMNS if defined; 80 otherwise

for tag; do
if [ "$word" = "1" ]; then tag="^$tag\$"; fi   # match full word


# talk to the gtags server

# print output as
#   line1: file_and_lineno_as_per_outfmt_defined_above
#   line2: snippet
#   line3: blank

# However, to retain the ability to grep over such outputs, we pad each
# line with $COLUMNS-length(line) whitespaces, and emit all three output
# lines as one long line.  Thus I coded my first darksome tidbit of python.


PYTHONPATH=$PYTHONPATH:/home/build/google3/tools/tags python -c "
import gtags
results = gtags.find_matching_tags('"$lang"', '"$tag"', $invocations)
for result in results:
  if result.filename_[0:2] == './':      # strip ./ which shows up in -i mode
    result.filename_ = result.filename_[2:]
  line1 = $outfmt
  pad1 = ' ' * ($columns - (len(line1) % $columns))
  line2 = result.snippet_
  pad2 = ' ' * ($columns - (len(line2) % $columns))
  print line1 + pad1 + $blue + line2 + $black + pad2 + (' ' * 80)"  \
| eval $restrictcmd   # restrict matches to a directory if specified; else cat
done
