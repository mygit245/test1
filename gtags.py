#!/usr/bin/python2.2
#
# Copyright 2004 Google Inc.
# All Rights Reserved.
# $Id: //depot/opensource/gtags/gtags.py#3 $

"""
A module for talking to gtags server via python.

This module does the equivalent of gtags.el, but is implemented into python
for possible integration into VIM or other editors.

Because those editors usually don't understand google3 programming conventions,
this module was written as a standard python module.

To use add this to your editor's config:
import sys
sys.path.append('/home/build/google3/tools/tags')
import gtags


"""

import socket
import signal
from cStringIO import StringIO

language_port_dict = {
  'c++' : 2223,
  'java' : 2224,
  'python' : 2225,
  'szl' : 2226 }

callgraph_port_dict = {
  'c++' : 2233,
  'java' : 2234,
  'python' : 2235,
  'szl' : 2236 }

gtags_definitions_host_dict = {
  'c++' : 'gtags.google.com',
  'java' : 'gtags.google.com',
  'python' : 'gtags.google.com',
  'szl' : 'gtags.google.com' }

gtags_callgraph_host_dict = {
  'c++' : 'gtags.google.com',
  'java' : 'gtags.google.com',
  'python' : 'gtags.google.com',
  'szl' : 'gtags.google.com' }

gtag_host = 'gtags.google.com'

gtag_default_port = 2223
callgraph_default_port = 2233

CONNECT_TIMEOUT = 5
DATA_TIMEOUT = 50

def alarm_handler(signum, frame):
  raise RuntimeError, "Timeout!"

# a class to store tags related information
class ETag:
  def __init__(self, filename, filesize):
    self.filename_ = filename
    self.filesize_ = filesize

  def set_tag(self, tag):
    self.tag_ = tag

  def set_lineno(self, lineno):
    self.lineno_ = lineno

  def set_fileoffset(self, offset):
    self.offset_ = offset

  def set_snippet(self, snippet):
    self.snippet_ = snippet

  def __str__(self):
    return 'tag: ' + self.tag_ + '\n' + 'filename: ' + self.filename_ + \
           '\nfilesize: ' + self.filesize_ + '\nlineno: ' + self.lineno_ + \
           '\noffset: ' + self.offset_ + '\nsnippet: ' + self.snippet_

def send_to_server(host, port, command):
  '''
  Sends command to specified port of gtags server and returns response.
  '''
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  address = socket.getaddrinfo(host, port, socket.AF_INET,
                               socket.SOCK_STREAM)
  signal.signal(signal.SIGALRM, alarm_handler)
  signal.alarm(CONNECT_TIMEOUT)
  s.connect(address[0][4])
  signal.alarm(0)
  signal.alarm(DATA_TIMEOUT)

  # need \r\n to match telnet protocol
  s.sendall(command + '\r\n')

  buffer = StringIO()
  data = s.recv(1024)

  # accumulate all data
  while data:
    buffer.write(data)
    data = s.recv(1024)
  signal.alarm(0)
  return buffer.getvalue()

def host_and_port(lang, callgraph):
  if callgraph:
    port = callgraph_port_dict.get(lang, callgraph_default_port)
    host = gtags_callgraph_host_dict.get(lang, gtag_host)
  else:
    port = language_port_dict.get(lang, gtag_default_port)
    host = gtags_definitions_host_dict.get(lang, gtag_host)
  return host,port

# returns a list of tags matching id for language language
def find_tag(language, id, callgraph = 0):
  return find_matching_tags(language, id + '$' , callgraph)

# read an S-expression (Lisp expression) from the string
# returns size of the sexp, and the sexp itself as a string
def read_sexp(data):
  sexp = ''
  i = 0
  while i < len(data) and data[i] != '(':
    i = i + 1
  # no sexp found!
  if i >= len(data):
    return (0, '')
  sexp += data[i]
  sexp_complete = None
  nestinglevels = 1
  while not sexp_complete:
    i = i + 1
    sexp += data[i]
    if data[i] == '"':
      (string_length, quoted_string) = read_quoted_string(data[i:], True)
      i += string_length
      sexp += quoted_string + '"'
    elif data[i] == '(':
      nestinglevels = nestinglevels + 1
    elif data[i] == ')':
      nestinglevels = nestinglevels - 1
    if nestinglevels == 0:
      sexp_complete = 1
  return (i, sexp)

# extracts a quoted string from the provided string
# returns size of the string and the string itself
def read_quoted_string(sexp, preserve_backslashes = 0):
  retval = ''
  i = 0
  while sexp[i] != '"':
    i = i + 1
  i = i + 1  # skip opening "
  while sexp[i] != '"':
    if sexp[i] == '\\':
      i = i + 1   # skip over \
      if preserve_backslashes:
        retval += '\\'
      retval += sexp[i]
    else:
      retval += sexp[i]
    i = i + 1
  return (i, retval)

# extracts an integer from the provided string
# returns the size of the int and the int itself
def read_int(exp):
  digits = ['1','2','3','4','5','6','7','8','9','0']
  retval = ''
  i = 0
  while not exp[i] in digits:
    i = i + 1
  while exp[i] in digits:
    retval += exp[i]
    i = i + 1
  return (i, retval)


def find_matching_tags(lang, id, callgraph = 0):
  return do_gtags_command(':', lang, id, callgraph)

def search_tag_snippets(lang, id, callgraph = 0):
  return do_gtags_command('$', lang, id, callgraph)

def list_tags_for_file(lang, filename):
  return do_gtags_command('@', lang, filename)

# returns a list of tags matching id for language language
def do_gtags_command(command, lang, id, callgraph = 0):
  host,port = host_and_port(lang, callgraph)
  tagsdata = send_to_server(host, port, command + id)

  tagslist = []
  finished = None

  # strip opening and closing parens
  tagsdata = tagsdata[tagsdata.find('(')+1:]
  tagsdata = tagsdata[:tagsdata.rfind(')')]
  while not finished:
    (i, sexp) = read_sexp(tagsdata)
    tagsdata = tagsdata[i+1:]    # skip the rest of this sexp
    if sexp == '':
      finished = 1
    else:
      (j, identifier) = read_quoted_string(sexp)
      while sexp[j] != '.':
        j = j + 1
      (discard, sexp) = read_sexp(sexp[j + 1:])
      (k, snippet) = read_quoted_string(sexp)
      sexp = sexp[k+1:]
      (k, filename) = read_quoted_string(sexp)
      sexp = sexp[k:]
      (k, filesize) = read_int(sexp)
      sexp = sexp[k:]
      (k, lineno) = read_int(sexp)
      sexp = sexp[k:]
      (k, offset) = read_int(sexp)
      tag = ETag(filename, filesize)
      tag.set_snippet(snippet)
      tag.set_tag(identifier)
      tag.set_lineno(lineno)
      tag.set_fileoffset(offset)
      tagslist.append(tag)

  return tagslist

# reload server
def server_reload(host, port, filename):
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  address = socket.getaddrinfo(host, port, socket.AF_INET,
                               socket.SOCK_STREAM)
  s.connect(address[0][4])
  s.sendall('!' + filename + '\r\n')
