#!/usr/bin/python2.2
#
# Copyright 2005 Google Inc. All Rights Reserved.

import os

tags_commands = {
  'TAGS' : r"find . -regex '.*\.\(cc\|c\|h\|lex\)' "
           r"| xargs /usr/pubsw/bin/etags -o TAGS -a",
  'JTAGS' : r"find . -regex '.*\.\(java\)' "
            r"| xargs /usr/pubsw/bin/etags -o JTAGS -a",
  'PTAGS' : r"find . -regex '.*\.\(py\)' "
            r"| xargs /usr/pubsw/bin/etags -o PTAGS -a",
  'TAGSC' : r"find . -regex '.*\.\(cc\|c\|h\|lex\)' | "
	 r"xargs rtags.py TAGSC",
  'JTAGSC' : r"find . -regex '.*\.\(java\)' | "
	 r"xargs rtags.py -JTAGSC",
  'PTAGSC' : r"find . -regex '.*\.\(py\)' | "
	 r"xargs rtags.py TAGSC",
  }

for target in [ 'TAGS', 'JTAGS', 'PTAGS', 
                  'TAGSC' , 'JTAGSC' , 'PTAGSC' ]:
  print 'generating tag file: ' + target
  os.system('rm -f %s' % target)
  result = os.system(tags_commands[target])
  if result != 0:
    print 'errors generating tag file for %s' % target
