## This is a boilerplate file for Google opensource projects.
## To make it useful, replace <<TEXT>> with actual text for your project.
## Also, look at comments with "## double hashes" to see if any are worth
## uncommenting or modifying.

%define	ver	%VERSION
%define	RELEASE	2
%define rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define	prefix	/usr

Name: %NAME
Summary: Server side tags
Version: %ver
Release: %rel
Group: Development/Tools
URL: http://goog-tags.sourceforge.net
License: BSD
Vendor: Google
Packager: Google Inc. <opensource@google.com>
Source: http://goog-tags.sourceforge.net/%{NAME}-%{PACKAGE_VERSION}.tar.gz
Distribution: Redhat 7 and above.
Buildroot: %{_tmppath}/%{name}-root
Prefix: %prefix

%description
Server side tags

This is an extension to GNU Emacs and X-Emacs TAGS functionality, with a
server-side component that narrows down the view of a potentially large TAGS
file and serves the narrowed view over the wire for better performance.

Emacs Lisp client, python client, and vim extensions supplied.

This package includes the client-side tags files.

%package server
Summary: gtags server
Group: Development/Tools

%description server
This package contains the gtags server.  If you are an end-user of gtags,
you won't need this package.


%changelog
    * Mon Oct 17 2005 <opensource@google.com>
    - First draft

%prep
%setup

%build
./configure
make prefix=%prefix

%install
rm -rf $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
/usr/lib/gtags

%files server
%defattr(-,root,root)
/usr/bin/gtags
/usr/bin/gtags.sh


