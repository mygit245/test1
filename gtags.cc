// Copyright 2005 Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// ---
// Author: Piaw Na
//
// TAGS server
// load etags style tags and then present a server interface
// to emacs
// We now load etags style tag lines only.
// etags file format is as follows:
// ^L
// filename,no of chars
// line snippet 0x7f identifier 0x01 lineno, char offset
//
// The protocol is straight forward --- we accept a single string
// with a one character command character, then the tag to look up
// We return in a LISP formatted style, all the relevant tag lines that have
// the tag, then we close the connection. (One tag per connection!)

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string>
#include <list>
#include <map>
#include <iostream>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>

#include "pcrecpp.h"

using std::multimap;
using std::list;
using std::cout;
using std::make_pair;

int tags_port =  2222;  // Port number to listen for connections
const int max_results = 2000; // Maximum number of results to return to clients
const int kMaxTagLen = 100;

struct TagsResult {
  string tag;              // tag
  string linerep;          // line to search on
  string filename;         // which file
  int filesize;            // size of files
  int lineno;              // line number
  int charno;              // char number
};

// ----------------------------------------------------------------------
// SimpleItoa()
//    Description: converts an integer to a string.
//    Faster than printf("%d").
//
//    Return value: string
// ----------------------------------------------------------------------
string SimpleItoa(int i) {
  // 10 digits and sign are good for 32-bit or smaller ints.
  // Longest is -2147483648.
  char local[11];
  char *p = local + sizeof(local);
  // Need to use uint32 instead of int32 to correctly handle
  // -2147483648.
  unsigned int n = i;
  if (i < 0)
    n = -i;
  do {
    *--p = '0' + n % 10;
    n /= 10;
  } while (n);
  if (i < 0)
    *--p = '-';
  return string(p, local + sizeof(local));
}

class TagsTable {
 private:
  multimap<string, TagsResult *> map_;
  multimap<string, TagsResult *> filemap_;

  bool ContainsRegexpChar(string tag) {
    static pcrecpp::RE rechars("[^a-zA-Z0-9\\-_]");

    return rechars.PartialMatch(tag);
  }

  void ClearMap(multimap<string, TagsResult*>& map) {
    for (multimap<string, TagsResult*>::iterator i = map.begin();
         i != map.end(); ++i) {
      delete i->second;
    }
    map.clear();
  }

 public:
  bool ReloadTagFile(string filename) {
    if (filename == "")
      return false;

    fprintf(stderr, "Loading %s\n", filename.c_str());
    ClearMap(map_);

    // Don't delete the values of filemap_ because they were in map_ too
    filemap_.clear();

    const int kMaxLine = 5000;
    FILE *file = fopen(filename.c_str(), "r");

    // does the file exist?
    if (file == NULL)
      return false;

    char line[kMaxLine];
    bool newfile = false;
    string curr_file;
    int curr_file_len = -1;
    int lines = 0;

    if (!file)
      return false;

    while (fgets(line, sizeof(line), file) != 0) {
      if (lines++ % 1000 == 0) {
        cout << '\r' << lines;
      }

      // check for control L
      if (line[0] == 0x0c) {
        newfile = true;
        continue;
      }

      // if the last line was a form feed, then this next section
      // tags a new file
      if (newfile) {
        char* filelen_idx = strchr(line, ',');

        assert(filelen_idx);
        curr_file = string(line, filelen_idx - line);
        curr_file_len = atoi(filelen_idx+1);
        newfile = false;
        continue;
      }

      // check for control-L at the end of a line --- indicates
      // corrupted etags file
      if (line[strlen(line)-2] == 0x0c) {
        fprintf(stderr, "Corrupted etags file at line %d\n", lines);
        newfile = true;
        continue;
      }

      // OK, not one of the above, assume we have a regular tags line
      TagsResult *tag = new TagsResult;

      assert(!curr_file.empty());
      assert(curr_file_len != -1);

      tag->filename = curr_file;
      tag->filesize = curr_file_len;

      char* cursor = line;  // pointer to what we're currently looking at
      char* char_search = strchr(cursor, '\177');

      if (!char_search) {
        fprintf(stderr, "Corrupted etags file at line %d\n",lines);
        continue; // skip this line, go on to the next
      }

      // pick up part 1
      tag->linerep = string(line, char_search - cursor);
      cursor = char_search + 1;

      // pick up the tag
      char_search = strchr(cursor, '\001');
      if (!char_search) {
        fprintf(stderr, "Corrupted etags file at line %d\n", lines);
        continue;
      }

      tag->tag = string(cursor, char_search - cursor);
      cursor = char_search + 1;

      // pick up line no
      char_search = strchr(cursor, ',');

      if (!char_search) {
        fprintf(stderr, "Corrupted etags file at line %d\n", lines);
        continue;
      }

      char lineno[kMaxLine];

      // we can prove that this is always true, but check it anyway
      assert(char_search - cursor < sizeof(lineno) - 1);

      strncpy(lineno, cursor, char_search - cursor);
      lineno[char_search - cursor] = '\0';
      tag->lineno = atoi(lineno);
      cursor = char_search + 1;

      // pick up char no
      tag->charno = atoi(cursor);

      map_.insert(make_pair<string, TagsResult *>(tag->tag, tag));
      filemap_.insert(make_pair<string, TagsResult *>(tag->filename, tag));
    }

    fclose(file);

    fprintf(stderr, "Read: %d lines\n", lines);

    return true;
  }

  // return snippet matches
  list<TagsResult *> FindSnippetMatches(string match) {
    list<TagsResult *> retval;
    int resultcount = 0;
    multimap<string, TagsResult *>::iterator pos;
    pcrecpp::RE snippetmatch(match);

    for (pos = map_.begin();
         pos != map_.end() && resultcount < max_results;
         pos++) {
      TagsResult* tag = pos->second;
      if (snippetmatch.PartialMatch(tag->linerep)) {
        retval.push_back(tag);
        resultcount++;
      }
    }

    return retval;
  }


  // return regexp matches
  list<TagsResult *> FindRegexpTags(string tag) {
    list<TagsResult *> retval;
    int resultcount = 0;
    multimap<string, TagsResult *>::iterator pos;
    bool prefix_mode = false;
    pcrecpp::RE retag(tag);

    if (ContainsRegexpChar(tag)) {
      pos = map_.begin();
    } else { // important special case
      pos = map_.find(tag);
      prefix_mode = true;
    }

    for (; pos != map_.end() && resultcount < max_results
                  && (!prefix_mode
                      || retag.PartialMatch(pos->first));
         pos++) {
      if (prefix_mode || retag.FullMatch(pos->first)) {
        retval.push_back(pos->second);
        resultcount++;
      }
    }

    return retval;
  }

  // return matching tags
  list<TagsResult *> FindTags(string tag) {
    list<TagsResult *> retval;

    for (multimap<string, TagsResult *>::iterator pos = map_.find(tag);
         pos != map_.end() && pos->first == tag; ++pos)
      retval.push_back(pos->second);

    return retval;
  }

  // return all tags in a particular file
  list<TagsResult *> FindTagsByFile(string filename) {
    list<TagsResult *> retval;
    int resultcount = 0;

    for (multimap<string, TagsResult *>::iterator pos = filemap_.find(filename);
         pos != filemap_.end() && pos->first == filename &&
         resultcount < max_results;
         pos++) {
      retval.push_back(pos->second);
      resultcount++;
    }

    return retval;
  }

};

TagsTable table;

static string EscapeQuotes(string s) {
  string retval;

  for (int i = 0; i < s.length(); i++)
    if (s[i] == '"')
      retval.append("\\\"");
    else if (s[i] == '\\')
      retval.append("\\\\");
    else
      retval.push_back(s[i]);
  return retval;
}


// we respond to network connections. Rather than use a command-language
// we use a prefix code with characters that aren't part of identifiers
// (e.g., :, ;, $, !) to distinguish between different kinds of tags

// We currently have two modes, exact search mode, and match mode
// exact mode takes a tag as input, and returns etags-compatible format
// output, making it easy to drop in to etags
// Match-mode (where identifiers are prefixed with a ":") returns a big
// elisp s-exp with format:
// ( "TAG-Name" . (linerep filename filesize lineno charno))
// which makes it easy to manipulate in Lisp (for various input into
// lisp functions like try-completion, etc.)
int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "Usage: gtags <tagfile> <port#>\n");
    exit(1);
  }

  if (!table.ReloadTagFile(argv[1])) {
    fprintf(stderr, "Could not load tag file: %s\n", argv[1]);
    exit(1);
  }

  signal(SIGPIPE, SIG_IGN); // ignore bad clients that cause signal errors

  int listen_socket = socket(PF_INET, SOCK_STREAM, 0);
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi(argv[2]));
  addr.sin_addr.s_addr = INADDR_ANY;

  if (bind(listen_socket, (struct sockaddr *)&addr,
           static_cast<socklen_t>(sizeof(addr))) == -1) {
    fprintf(stderr, "Couldn't bind to port: %s", argv[2]);
    exit(1);
  }

  int server_socket = listen(listen_socket, 3);

  if (server_socket == -1) {
    perror("Error");
    exit(1);
  }

  fprintf(stderr, "listen done\n");

  while (true) {
    struct sockaddr a_addr;
    socklen_t addrlen;
    int connected_socket = accept(listen_socket, &a_addr, &addrlen);

    if (connected_socket == -1) {
      perror("Error");
      continue;
    }

    fprintf(stderr, "accepted on socket %d\n", connected_socket);

    if (connected_socket != -1) {
      char tag[kMaxTagLen];

      int bytesread = read(connected_socket, tag, sizeof(tag));

      fprintf(stderr, "bytes read: %d\n", bytesread);

      // strip off cr and lf - telnet protocol
      if (bytesread >= 2 && tag[bytesread-1] == '\n'
          && tag[bytesread-2] == '\r') {
        // null terminate the string, knocking off \r\n
        tag[bytesread-2] = '\0';
        fprintf(stderr, "Tag: %s\n", tag);

        string output;
        list<TagsResult *> results;
        if (*tag == ':') {
          // doing regexp search
          results =  table.FindRegexpTags(string(tag+1));
        } else if (*tag == '$') {
          // search snippets
          results = table.FindSnippetMatches(string(tag+1));
        } else if (*tag == '@') {
          // return all tags in a particular file
          results = table.FindTagsByFile(string(tag+1));
        }

        string prevfile;
        // respond in lisp strings
        output.push_back('(');

        for (list<TagsResult *>::iterator i = results.begin();
             i != results.end(); ++i) {
          output.push_back('(');
          output.append("\""+(*i)->tag+"\"");
          output.append(" . (");
          output.append("\""+EscapeQuotes((*i)->linerep)+"\"");
          output.append(" \""+(*i)->filename+"\"");
          output.push_back(' ');
          output.append(SimpleItoa((*i)->filesize));
          output.push_back(' ');
          output.append(SimpleItoa((*i)->lineno));
          output.push_back(' ');
          output.append(SimpleItoa((*i)->charno));
          output.append(")) ");
        }
        output.push_back(')');

        const char* outbuf = output.c_str();
        int towrite = output.length();
        int written = 0;

        while (written < towrite) {
          int wrote = write(connected_socket, outbuf, towrite);
          if (wrote <= 0)
            break;   // something went wrong, just close the connection
          written += wrote;
          outbuf += wrote;
        }
      }
      close(connected_socket);
    }
  }
}
